module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  theme: {
    extend: {
      colors: {
        slate: "#201335",
        pantone: {
          light: "#F0782D",
          DEFAULT: "#E56210",
          dark: "#D2590F",
        },
        cultured: "#f5f5f5",
        mint: "#EBFEF8",
      },
    },
  },
  variants: {},
  plugins: [],
}
