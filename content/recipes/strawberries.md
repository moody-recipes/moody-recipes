---
title: Strawberries
author: The Moody Recipes Team
date: 2020-12-29T00:51:35.574Z
difficulty: easy
duration: 10
mood:
  - happy
  - sad
  - sick
ingredients: "* 500g Strawberries"
desc_short: These strawberries are sure to boost your mood!
directions: |-
  1. Wash your strawberries before using them.
  2. Remove the stem.
  3. Cut each strawberry in half.
  4. Add some whipped cream on top.
  5. Done!
---
